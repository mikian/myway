const express = require('express')
const app = express()
const elasticsearch = require('elasticsearch')

const client = new elasticsearch.Client()

app.get('/', function(req, res) {
  console.log(req.query)
  client.search({
    index: 'tfl-stops',
    body: {
      sort: {
        '_geo_distance': {
          location: {
            lat: req.query.lat,
            lon: req.query.lon
          },
          order: 'asc',
          unit: 'km'
        }
      }
    }
  }, function(error, response, status) {
    if (error) {
      res.send(error)
    } else {
      res.send(response.hits.hits[0]._source.name)
    }
  })
})

app.listen(3000, function () {})
