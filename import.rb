#!/usr/bin/env ruby

# Import TfL Tube Stations to Elasticsearch
require 'elasticsearch'
require 'json'
require 'net/http'
require 'pry'

client = Elasticsearch::Client.new log: true
client.indices.delete(index: 'tfl-stops') if client.indices.exists?(index: 'tfl-stops')
client.indices.create(
  index: 'tfl-stops',
  body: {
    mappings: {
      document: {
        properties: {
          name: { type: 'string' },
          location: { type: 'geo_point' }
        }
      }
    }
  }
)

url = URI("https://api.tfl.gov.uk/StopPoint/Type/NaptanMetroStation")
response = Net::HTTP.get(url)
data = JSON.parse(response)

data.each do |stop|
  client.index index: 'tfl-stops',
               type: 'document',
               body: {
                 name: stop['commonName'],
                 location: { lon: stop['lon'], lat: stop['lat'] }
               }
end

puts 'Done.'
