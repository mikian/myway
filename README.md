# MyWay

A very, very simple playground to play with Node.js Express and ElasticSearch.

## How to Run

1. Make sure you have local elasticsearch running. MyWay uses `tfl-stops`
   index that it will recreate always, so make sure you don't have already that
   index in use somewhere else.
2. Run import process:

       ./import.rb

3. Start local webserver:

       node server.js

4. Make queries:

       http 'http://localhost:3000?lat=51.403767&lon=-3.055787'

## Purpose

This is a very simple testing project to play quickly with node.js express
and elasticsearch. It imports TfL Stops and imports them to ES including
geo_point. This allows to search nearest station via simple API.

**Known Issues**

For some reason, the location does not exactly work. Even though data seems to
have correct coordinates and it is imported to ES, when searching results are
bit weird. However, it works in scope of this silly little app, i.e. making
queries to ES via express app.
